﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainWindow
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TitleBanner = New System.Windows.Forms.Label()
        Me.CalcDisplay = New System.Windows.Forms.Label()
        Me.groupOperators = New System.Windows.Forms.GroupBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnMemoryMinus = New System.Windows.Forms.Button()
        Me.btnSubtract = New System.Windows.Forms.Button()
        Me.btnMemoryPlus = New System.Windows.Forms.Button()
        Me.btnMultiply = New System.Windows.Forms.Button()
        Me.btnMemoryClear = New System.Windows.Forms.Button()
        Me.btnDivide = New System.Windows.Forms.Button()
        Me.btnMemoryRecall = New System.Windows.Forms.Button()
        Me.groupFunctions = New System.Windows.Forms.GroupBox()
        Me.btnPlusMinus = New System.Windows.Forms.Button()
        Me.btnPercent = New System.Windows.Forms.Button()
        Me.btnSquareRoot = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.btn0 = New System.Windows.Forms.Button()
        Me.btnDecimal = New System.Windows.Forms.Button()
        Me.btnEquals = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.groupOperators.SuspendLayout()
        Me.groupFunctions.SuspendLayout()
        Me.SuspendLayout()
        '
        'TitleBanner
        '
        Me.TitleBanner.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TitleBanner.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.06283!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TitleBanner.Location = New System.Drawing.Point(65, 32)
        Me.TitleBanner.Name = "TitleBanner"
        Me.TitleBanner.Size = New System.Drawing.Size(680, 52)
        Me.TitleBanner.TabIndex = 0
        Me.TitleBanner.Text = "My Calculator"
        Me.TitleBanner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CalcDisplay
        '
        Me.CalcDisplay.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CalcDisplay.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.CalcDisplay.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CalcDisplay.Font = New System.Drawing.Font("Consolas", 18.09424!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CalcDisplay.Location = New System.Drawing.Point(65, 109)
        Me.CalcDisplay.Name = "CalcDisplay"
        Me.CalcDisplay.Size = New System.Drawing.Size(680, 68)
        Me.CalcDisplay.TabIndex = 1
        Me.CalcDisplay.Text = "0."
        Me.CalcDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'groupOperators
        '
        Me.groupOperators.Controls.Add(Me.btnAdd)
        Me.groupOperators.Controls.Add(Me.btnMemoryMinus)
        Me.groupOperators.Controls.Add(Me.btnSubtract)
        Me.groupOperators.Controls.Add(Me.btnMemoryPlus)
        Me.groupOperators.Controls.Add(Me.btnMultiply)
        Me.groupOperators.Controls.Add(Me.btnMemoryClear)
        Me.groupOperators.Controls.Add(Me.btnDivide)
        Me.groupOperators.Controls.Add(Me.btnMemoryRecall)
        Me.groupOperators.Location = New System.Drawing.Point(532, 206)
        Me.groupOperators.Name = "groupOperators"
        Me.groupOperators.Size = New System.Drawing.Size(226, 367)
        Me.groupOperators.TabIndex = 2
        Me.groupOperators.TabStop = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.Silver
        Me.btnAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdd.Location = New System.Drawing.Point(16, 296)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(92, 51)
        Me.btnAdd.TabIndex = 22
        Me.btnAdd.Text = "+"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnMemoryMinus
        '
        Me.btnMemoryMinus.BackColor = System.Drawing.Color.LightSteelBlue
        Me.btnMemoryMinus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.047121!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMemoryMinus.Location = New System.Drawing.Point(121, 296)
        Me.btnMemoryMinus.Name = "btnMemoryMinus"
        Me.btnMemoryMinus.Size = New System.Drawing.Size(92, 51)
        Me.btnMemoryMinus.TabIndex = 21
        Me.btnMemoryMinus.Text = "M−"
        Me.btnMemoryMinus.UseVisualStyleBackColor = False
        '
        'btnSubtract
        '
        Me.btnSubtract.BackColor = System.Drawing.Color.Silver
        Me.btnSubtract.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubtract.Location = New System.Drawing.Point(16, 205)
        Me.btnSubtract.Name = "btnSubtract"
        Me.btnSubtract.Size = New System.Drawing.Size(92, 51)
        Me.btnSubtract.TabIndex = 20
        Me.btnSubtract.Text = "−"
        Me.btnSubtract.UseVisualStyleBackColor = False
        '
        'btnMemoryPlus
        '
        Me.btnMemoryPlus.BackColor = System.Drawing.Color.LightSteelBlue
        Me.btnMemoryPlus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.047121!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMemoryPlus.Location = New System.Drawing.Point(121, 205)
        Me.btnMemoryPlus.Name = "btnMemoryPlus"
        Me.btnMemoryPlus.Size = New System.Drawing.Size(92, 51)
        Me.btnMemoryPlus.TabIndex = 19
        Me.btnMemoryPlus.Text = "M+"
        Me.btnMemoryPlus.UseVisualStyleBackColor = False
        '
        'btnMultiply
        '
        Me.btnMultiply.BackColor = System.Drawing.Color.Silver
        Me.btnMultiply.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMultiply.Location = New System.Drawing.Point(16, 115)
        Me.btnMultiply.Name = "btnMultiply"
        Me.btnMultiply.Size = New System.Drawing.Size(92, 51)
        Me.btnMultiply.TabIndex = 18
        Me.btnMultiply.Text = "×"
        Me.btnMultiply.UseVisualStyleBackColor = False
        '
        'btnMemoryClear
        '
        Me.btnMemoryClear.BackColor = System.Drawing.Color.LightSteelBlue
        Me.btnMemoryClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.047121!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMemoryClear.Location = New System.Drawing.Point(121, 115)
        Me.btnMemoryClear.Name = "btnMemoryClear"
        Me.btnMemoryClear.Size = New System.Drawing.Size(92, 51)
        Me.btnMemoryClear.TabIndex = 17
        Me.btnMemoryClear.Text = "MC"
        Me.btnMemoryClear.UseVisualStyleBackColor = False
        '
        'btnDivide
        '
        Me.btnDivide.BackColor = System.Drawing.Color.Silver
        Me.btnDivide.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDivide.Location = New System.Drawing.Point(16, 27)
        Me.btnDivide.Name = "btnDivide"
        Me.btnDivide.Size = New System.Drawing.Size(92, 51)
        Me.btnDivide.TabIndex = 16
        Me.btnDivide.Text = "÷"
        Me.btnDivide.UseVisualStyleBackColor = False
        '
        'btnMemoryRecall
        '
        Me.btnMemoryRecall.BackColor = System.Drawing.Color.LightSteelBlue
        Me.btnMemoryRecall.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.047121!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMemoryRecall.Location = New System.Drawing.Point(121, 27)
        Me.btnMemoryRecall.Name = "btnMemoryRecall"
        Me.btnMemoryRecall.Size = New System.Drawing.Size(92, 51)
        Me.btnMemoryRecall.TabIndex = 15
        Me.btnMemoryRecall.Text = "MR"
        Me.btnMemoryRecall.UseVisualStyleBackColor = False
        '
        'groupFunctions
        '
        Me.groupFunctions.Controls.Add(Me.btnClear)
        Me.groupFunctions.Controls.Add(Me.btnPlusMinus)
        Me.groupFunctions.Controls.Add(Me.btnPercent)
        Me.groupFunctions.Controls.Add(Me.btnSquareRoot)
        Me.groupFunctions.Location = New System.Drawing.Point(51, 206)
        Me.groupFunctions.Name = "groupFunctions"
        Me.groupFunctions.Size = New System.Drawing.Size(128, 367)
        Me.groupFunctions.TabIndex = 3
        Me.groupFunctions.TabStop = False
        '
        'btnPlusMinus
        '
        Me.btnPlusMinus.BackColor = System.Drawing.Color.Silver
        Me.btnPlusMinus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPlusMinus.Location = New System.Drawing.Point(14, 293)
        Me.btnPlusMinus.Name = "btnPlusMinus"
        Me.btnPlusMinus.Size = New System.Drawing.Size(100, 51)
        Me.btnPlusMinus.TabIndex = 18
        Me.btnPlusMinus.Text = "+/−"
        Me.btnPlusMinus.UseVisualStyleBackColor = False
        '
        'btnPercent
        '
        Me.btnPercent.BackColor = System.Drawing.Color.Silver
        Me.btnPercent.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPercent.Location = New System.Drawing.Point(14, 203)
        Me.btnPercent.Name = "btnPercent"
        Me.btnPercent.Size = New System.Drawing.Size(100, 51)
        Me.btnPercent.TabIndex = 17
        Me.btnPercent.Text = "%"
        Me.btnPercent.UseVisualStyleBackColor = False
        '
        'btnSquareRoot
        '
        Me.btnSquareRoot.BackColor = System.Drawing.Color.Silver
        Me.btnSquareRoot.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.916231!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSquareRoot.Location = New System.Drawing.Point(14, 115)
        Me.btnSquareRoot.Name = "btnSquareRoot"
        Me.btnSquareRoot.Size = New System.Drawing.Size(100, 51)
        Me.btnSquareRoot.TabIndex = 16
        Me.btnSquareRoot.Text = "SQRT"
        Me.btnSquareRoot.UseVisualStyleBackColor = False
        '
        'btn9
        '
        Me.btn9.BackColor = System.Drawing.Color.Gray
        Me.btn9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn9.ForeColor = System.Drawing.Color.White
        Me.btn9.Location = New System.Drawing.Point(416, 233)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(92, 51)
        Me.btn9.TabIndex = 4
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = False
        '
        'btn8
        '
        Me.btn8.BackColor = System.Drawing.Color.Gray
        Me.btn8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn8.ForeColor = System.Drawing.Color.White
        Me.btn8.Location = New System.Drawing.Point(311, 233)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(92, 51)
        Me.btn8.TabIndex = 5
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = False
        '
        'btn7
        '
        Me.btn7.BackColor = System.Drawing.Color.Gray
        Me.btn7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn7.ForeColor = System.Drawing.Color.White
        Me.btn7.Location = New System.Drawing.Point(204, 233)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(92, 51)
        Me.btn7.TabIndex = 6
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = False
        '
        'btn6
        '
        Me.btn6.BackColor = System.Drawing.Color.Gray
        Me.btn6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn6.ForeColor = System.Drawing.Color.White
        Me.btn6.Location = New System.Drawing.Point(416, 321)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(92, 51)
        Me.btn6.TabIndex = 7
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = False
        '
        'btn5
        '
        Me.btn5.BackColor = System.Drawing.Color.Gray
        Me.btn5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn5.ForeColor = System.Drawing.Color.White
        Me.btn5.Location = New System.Drawing.Point(311, 321)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(92, 51)
        Me.btn5.TabIndex = 8
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = False
        '
        'btn4
        '
        Me.btn4.BackColor = System.Drawing.Color.Gray
        Me.btn4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn4.ForeColor = System.Drawing.Color.White
        Me.btn4.Location = New System.Drawing.Point(204, 321)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(92, 51)
        Me.btn4.TabIndex = 9
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = False
        '
        'btn3
        '
        Me.btn3.BackColor = System.Drawing.Color.Gray
        Me.btn3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn3.ForeColor = System.Drawing.Color.White
        Me.btn3.Location = New System.Drawing.Point(416, 411)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(92, 51)
        Me.btn3.TabIndex = 10
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = False
        '
        'btn2
        '
        Me.btn2.BackColor = System.Drawing.Color.Gray
        Me.btn2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn2.ForeColor = System.Drawing.Color.White
        Me.btn2.Location = New System.Drawing.Point(311, 411)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(92, 51)
        Me.btn2.TabIndex = 11
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = False
        '
        'btn1
        '
        Me.btn1.BackColor = System.Drawing.Color.Gray
        Me.btn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn1.ForeColor = System.Drawing.Color.White
        Me.btn1.Location = New System.Drawing.Point(204, 411)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(92, 51)
        Me.btn1.TabIndex = 12
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = False
        '
        'btn0
        '
        Me.btn0.BackColor = System.Drawing.Color.Gray
        Me.btn0.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn0.ForeColor = System.Drawing.Color.White
        Me.btn0.Location = New System.Drawing.Point(204, 502)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(92, 51)
        Me.btn0.TabIndex = 15
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = False
        '
        'btnDecimal
        '
        Me.btnDecimal.BackColor = System.Drawing.Color.Gray
        Me.btnDecimal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDecimal.ForeColor = System.Drawing.Color.White
        Me.btnDecimal.Location = New System.Drawing.Point(311, 502)
        Me.btnDecimal.Name = "btnDecimal"
        Me.btnDecimal.Size = New System.Drawing.Size(92, 51)
        Me.btnDecimal.TabIndex = 14
        Me.btnDecimal.Text = "."
        Me.btnDecimal.UseVisualStyleBackColor = False
        '
        'btnEquals
        '
        Me.btnEquals.BackColor = System.Drawing.Color.Gray
        Me.btnEquals.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.93194!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEquals.ForeColor = System.Drawing.Color.White
        Me.btnEquals.Location = New System.Drawing.Point(416, 502)
        Me.btnEquals.Name = "btnEquals"
        Me.btnEquals.Size = New System.Drawing.Size(92, 51)
        Me.btnEquals.TabIndex = 13
        Me.btnEquals.Text = "="
        Me.btnEquals.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.IndianRed
        Me.btnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.047121!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.ForeColor = System.Drawing.Color.White
        Me.btnClear.Location = New System.Drawing.Point(14, 29)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(100, 51)
        Me.btnClear.TabIndex = 16
        Me.btnClear.Text = "CLR"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'MainWindow
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(811, 602)
        Me.Controls.Add(Me.btn0)
        Me.Controls.Add(Me.btnDecimal)
        Me.Controls.Add(Me.btnEquals)
        Me.Controls.Add(Me.btn1)
        Me.Controls.Add(Me.btn2)
        Me.Controls.Add(Me.btn3)
        Me.Controls.Add(Me.btn4)
        Me.Controls.Add(Me.btn5)
        Me.Controls.Add(Me.btn6)
        Me.Controls.Add(Me.btn7)
        Me.Controls.Add(Me.btn8)
        Me.Controls.Add(Me.btn9)
        Me.Controls.Add(Me.groupFunctions)
        Me.Controls.Add(Me.groupOperators)
        Me.Controls.Add(Me.CalcDisplay)
        Me.Controls.Add(Me.TitleBanner)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(837, 671)
        Me.MinimumSize = New System.Drawing.Size(837, 671)
        Me.Name = "MainWindow"
        Me.Text = "Calculator"
        Me.groupOperators.ResumeLayout(False)
        Me.groupFunctions.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TitleBanner As System.Windows.Forms.Label
    Friend WithEvents CalcDisplay As System.Windows.Forms.Label
    Friend WithEvents groupOperators As System.Windows.Forms.GroupBox
    Friend WithEvents groupFunctions As System.Windows.Forms.GroupBox
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents btn0 As System.Windows.Forms.Button
    Friend WithEvents btnDecimal As System.Windows.Forms.Button
    Friend WithEvents btnEquals As System.Windows.Forms.Button
    Friend WithEvents btnPlusMinus As System.Windows.Forms.Button
    Friend WithEvents btnPercent As System.Windows.Forms.Button
    Friend WithEvents btnSquareRoot As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnMemoryMinus As System.Windows.Forms.Button
    Friend WithEvents btnSubtract As System.Windows.Forms.Button
    Friend WithEvents btnMemoryPlus As System.Windows.Forms.Button
    Friend WithEvents btnMultiply As System.Windows.Forms.Button
    Friend WithEvents btnMemoryClear As System.Windows.Forms.Button
    Friend WithEvents btnDivide As System.Windows.Forms.Button
    Friend WithEvents btnMemoryRecall As System.Windows.Forms.Button
    Friend WithEvents btnClear As System.Windows.Forms.Button

End Class
