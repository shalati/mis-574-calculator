﻿Option Explicit On
Option Strict On

Imports System.Math
Imports System.Text.RegularExpressions

Public Class MainWindow

    Private dblMemory As Double
    Private dblResult As Double
    Private dblCurrentNumber As Double
    Private blnStartNewNumber As Boolean
    Private strLastMathOperator As String

    ''' <summary>
    ''' Clears the Calculator
    ''' </summary>
    Private Sub resetCalculator()
        dblResult = 0
        dblCurrentNumber = 0
        blnStartNewNumber = True
        strLastMathOperator = "Clear"
        CalcDisplay.Text = "0"
    End Sub

    ''' <summary>
    ''' Builds the calculation display from string inputs, one entry at a time.
    ''' </summary>
    Private Sub buildNumber(ByVal strNumber As String)
        Dim strTempNumber As String

        ' Are we starting over?
        If blnStartNewNumber Then
            ' Allow the user to start with a decimal.
            If String.Equals(strNumber, ".") Then
                strTempNumber = "0."
            Else
                strTempNumber = strNumber
            End If
        Else
            ' Handle plus-minus.
            If String.Equals(strNumber, "-") Then
                strTempNumber = negateNumberString(CalcDisplay.Text)
            Else
                strTempNumber = CalcDisplay.Text & strNumber
            End If
        End If

        ' Remove leading zeros.
        strTempNumber = removeLeadingZeros(strTempNumber)

        ' Try to parse the double.
        Try
            dblCurrentNumber = Convert.ToDouble(strTempNumber)
        Catch ex As Exception
            Return ' The entry didn't parse.
        End Try
        CalcDisplay.Text = strTempNumber
        blnStartNewNumber = False
    End Sub

    ''' <summary>
    ''' Handles operation actions, including ADD, SUBTRACT, ETC.
    ''' Also handles EQUALS.
    ''' </summary>
    Private Sub handleOperator(ByVal strOperation As String)
        ' Handle any previous operations in case the user presses "1 + 2 + ".
        Select Case strLastMathOperator.ToUpper
            Case "ADD"
                dblResult = dblResult + dblCurrentNumber
            Case "SUBTRACT"
                dblResult = dblResult - dblCurrentNumber
            Case "MULTIPLY"
                dblResult = dblResult * dblCurrentNumber
            Case "DIVIDE"
                dblResult = dblResult / dblCurrentNumber
            Case Else
                dblResult = dblCurrentNumber
        End Select

        ' Show the current result.
        dblCurrentNumber = dblResult
        finalizeCurrentNumber()

        ' Save last operator.
        strLastMathOperator = strOperation
    End Sub

    ''' <summary>
    ''' Finishes entry of the current number.  Reprints it from the double value.
    ''' </summary>
    Private Sub finalizeCurrentNumber()
        CalcDisplay.Text = dblCurrentNumber.ToString
        blnStartNewNumber = True
    End Sub

    ''' <summary>
    ''' Removes only unnecessary leading zeros from a number string.
    ''' Can handle negative numbers.
    ''' </summary>
    Private Function removeLeadingZeros(ByVal strNumber As String) As String
        Return New Regex("^(-?)0+([0-9])(.*)").Replace(strNumber, "$1$2$3")
    End Function

    ''' <summary>
    ''' Negates a number string.
    ''' </summary>
    Private Function negateNumberString(ByVal strNumber As String) As String
        Return If(strNumber.StartsWith("-"), strNumber.Substring(1), "-" & strNumber)
    End Function

    ''' <summary>
    ''' Updates the Memory Indicator
    ''' </summary>
    Private Sub updateMemoryIndicator()
        If dblMemory.Equals(0.0) Then
            btnMemoryRecall.ForeColor = Color.Black
        Else
            btnMemoryRecall.ForeColor = Color.Yellow
        End If
    End Sub
    ' ----------------------------------------------------------------------------------
    ' Event Handlers
    ' ----------------------------------------------------------------------------------

    ''' <summary>
    ''' When the form loads.
    ''' </summary>
    Private Sub MainWindow_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        resetCalculator()
    End Sub

    ''' <summary>
    ''' User presses CLEAR
    ''' </summary>
    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        resetCalculator()
    End Sub

    ''' <summary>
    ''' User presses 0
    ''' </summary>
    Private Sub btn0_Click(sender As Object, e As EventArgs) Handles btn0.Click
        buildNumber("0")
    End Sub

    ''' <summary>
    ''' User presses 1
    ''' </summary>
    Private Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
        buildNumber("1")
    End Sub

    ''' <summary>
    ''' User presses 2
    ''' </summary>
    Private Sub btn2_Click(sender As Object, e As EventArgs) Handles btn2.Click
        buildNumber("2")
    End Sub

    ''' <summary>
    ''' User presses 3
    ''' </summary>
    Private Sub btn3_Click(sender As Object, e As EventArgs) Handles btn3.Click
        buildNumber("3")
    End Sub

    ''' <summary>
    ''' User presses 4
    ''' </summary>
    Private Sub btn4_Click(sender As Object, e As EventArgs) Handles btn4.Click
        buildNumber("4")
    End Sub

    ''' <summary>
    ''' User presses 5
    ''' </summary>
    Private Sub btn5_Click(sender As Object, e As EventArgs) Handles btn5.Click
        buildNumber("5")
    End Sub

    ''' <summary>
    ''' User presses 6
    ''' </summary>
    Private Sub btn6_Click(sender As Object, e As EventArgs) Handles btn6.Click
        buildNumber("6")
    End Sub

    ''' <summary>
    ''' User presses 7
    ''' </summary>
    Private Sub btn7_Click(sender As Object, e As EventArgs) Handles btn7.Click
        buildNumber("7")
    End Sub

    ''' <summary>
    ''' User presses 8
    ''' </summary>
    Private Sub btn8_Click(sender As Object, e As EventArgs) Handles btn8.Click
        buildNumber("8")
    End Sub

    ''' <summary>
    ''' User presses 9
    ''' </summary>
    Private Sub btn9_Click(sender As Object, e As EventArgs) Handles btn9.Click
        buildNumber("9")
    End Sub

    ''' <summary>
    ''' User presses DECIMAL
    ''' </summary>
    Private Sub btnDecimal_Click(sender As Object, e As EventArgs) Handles btnDecimal.Click
        buildNumber(".")
    End Sub

    ''' <summary>
    ''' User presses PLUS/MINUS
    ''' </summary>
    Private Sub btnPlusMinus_Click(sender As Object, e As EventArgs) Handles btnPlusMinus.Click
        ' Are we starting a new number?
        If blnStartNewNumber Then
            If dblCurrentNumber.Equals(0) Then
                ' Allow the user to start a new number with - by showing -0
                CalcDisplay.Text = "-0"
                blnStartNewNumber = False
            Else
                ' Otherwise, just multiply by -1
                dblCurrentNumber *= -1
                CalcDisplay.Text = dblCurrentNumber.ToString
            End If
        Else
            ' Handle the rest in buildNumber
            buildNumber("-")
        End If
    End Sub

    ''' <summary>
    ''' User presses SQRT
    ''' </summary>
    Private Sub btnSquareRoot_Click(sender As Object, e As EventArgs) Handles btnSquareRoot.Click
        ' Take the square root, display the new value and start a new number.
        dblCurrentNumber = Sqrt(dblCurrentNumber)
        finalizeCurrentNumber()
    End Sub

    ''' <summary>
    ''' User presses PERCENT
    ''' </summary>
    Private Sub btnPercent_Click(sender As Object, e As EventArgs) Handles btnPercent.Click
        ' Divide by 100, display the new value and start a new number.
        dblCurrentNumber = dblCurrentNumber / 100
        finalizeCurrentNumber()
    End Sub

    ''' <summary>
    ''' User presses ADD
    ''' </summary>
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        handleOperator("Add")
    End Sub

    ''' <summary>
    ''' User presses SUBTRACT
    ''' </summary>
    Private Sub btnSubtract_Click(sender As Object, e As EventArgs) Handles btnSubtract.Click
        handleOperator("Subtract")
    End Sub

    ''' <summary>
    ''' User presses MULTIPLY
    ''' </summary>
    Private Sub btnMultiply_Click(sender As Object, e As EventArgs) Handles btnMultiply.Click
        handleOperator("Multiply")
    End Sub

    ''' <summary>
    ''' User presses DIVIDE
    ''' </summary>
    Private Sub btnDivide_Click(sender As Object, e As EventArgs) Handles btnDivide.Click
        handleOperator("Divide")
    End Sub

    ''' <summary>
    ''' User presses EQUALS
    ''' </summary>
    Private Sub btnEquals_Click(sender As Object, e As EventArgs) Handles btnEquals.Click
        handleOperator("Equals")
    End Sub

    ''' <summary>
    ''' User presses MR
    ''' </summary>
    Private Sub btnMemoryRecall_Click(sender As Object, e As EventArgs) Handles btnMemoryRecall.Click
        ' Apply the value from memory, display it and start a new number.
        dblCurrentNumber = dblMemory
        finalizeCurrentNumber()
    End Sub

    ''' <summary>
    ''' User presses MC
    ''' </summary>
    Private Sub btnMemoryClear_Click(sender As Object, e As EventArgs) Handles btnMemoryClear.Click
        dblMemory = 0.0
        updateMemoryIndicator()
    End Sub

    ''' <summary>
    ''' User presses M+
    ''' </summary>
    Private Sub btnMemoryPlus_Click(sender As Object, e As EventArgs) Handles btnMemoryPlus.Click
        dblMemory += dblCurrentNumber
        finalizeCurrentNumber()
        updateMemoryIndicator()
    End Sub

    ''' <summary>
    ''' User presses M-
    ''' </summary>
    Private Sub btnMemoryMinus_Click(sender As Object, e As EventArgs) Handles btnMemoryMinus.Click
        dblMemory -= dblCurrentNumber
        finalizeCurrentNumber()
        updateMemoryIndicator()
    End Sub

End Class
